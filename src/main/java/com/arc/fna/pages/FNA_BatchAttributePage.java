package com.arc.fna.pages;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import com.arc.fna.utils.PropertyReader;
import com.arc.fna.utils.SkySiteUtils;
import com.arc.fna.utils.randomFileName;
import com.arcautoframe.utils.Log;

public class FNA_BatchAttributePage extends LoadableComponent<FNA_BatchAttributePage> {
	 WebDriver driver;
	    private  boolean isPageLoaded;
	@Override
	protected void load() {
		// TODO Auto-generated method stub
		isPageLoaded=true;
	}

	@Override
	protected void isLoaded() throws Error {
		// TODO Auto-generated method stub
		if (!isPageLoaded)
		{
			Assert.fail();
		}
	}
	/**
	 * Declaring constructor for initializing web elements using PageFactory class.
	 * @param driver
	 */
	public FNA_BatchAttributePage(WebDriver driver){
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}
	@FindBy(css = "#Button1")
  //@FindBy(css = "#rgtpnlMore")
   // WebElement moreoption;
	// @FindBy(css="#rgtpnlMore")
	  @CacheLookup
	  WebElement moreoption;
	 @FindBy(xpath="//*[contains(text(),'Modify attributes')]")
	 
	  WebElement modifyattribute;
	/**Method to select a file and open modify attirbute  window
	 * Scripted by tarak
	 */
	public void selectFileandModifyAttribute(String Foldername) {
		 driver.switchTo().defaultContent();
	 		Log.message("Switched to default content");
	 		SkySiteUtils.waitTill(5000);
	 		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
	 		Log.message("Switched to frame");
	 		SkySiteUtils.waitTill(4000);
	 	
	 		int Count_no = driver.findElements(By.xpath("//*[@id='treeboxbox_tree_wrap_container']/div/table/tbody/tr[2]/td[2]/table/tbody/tr/td[2]/table/tbody/tr/td[4]/span")).size();
	 		Log.message("the number folder present are " + Count_no);
	 		SkySiteUtils.waitTill(5000);
	 		int k = 0;
	 		for (k = 1; k <= Count_no; k++) {

	 			String foldernamepresent = driver.findElement(By.xpath("(//*[@id='treeboxbox_tree_wrap_container']/div/table/tbody/tr[2]/td[2]/table/tbody/tr/td[2]/table/tbody/tr/td[4]/span)["+k+"]")).getText();
	 			if (foldernamepresent.contains(Foldername)) {
	 				Log.message("Required folder is present at " + k + " position in the tree");
	 				driver.findElement(By.xpath("(//*[@id='treeboxbox_tree_wrap_container']/div/table/tbody/tr[2]/td[2]/table/tbody/tr/td[2]/table/tbody/tr/td[4]/span)["+ k + "]")).click();
	 				Log.message("Folder is selected");
	 				SkySiteUtils.waitTill(5000);
	 				break;
	 			}
	 		}
	 	//	String Deletedfilename= PropertyReader.getProperty("DeleteFilename");
	 		int Itemlist = driver.findElements(By.xpath("//*[@id='divGrid']/div[2]/table/tbody/tr/td[1]/img")).size();
			Log.message("the number files present are " + Itemlist);
			SkySiteUtils.waitTill(5000);
			int j = 0;
			for (j = 1; j <= Itemlist; j++) {

					driver.findElement(By.xpath("(//*[@id='divGrid']/div[2]/table/tbody/tr/td[1])["+j+"]")).click();
					Log.message("File is selected ");
					SkySiteUtils.waitTill(4000);
					break;
		     }
			SkySiteUtils.waitTill(4000);
			moreoption.click();
			Log.message("more option is clicked ");
			SkySiteUtils.waitTill(4000);
			SkySiteUtils.waitForElement(driver, modifyattribute, 50);
			
			JavascriptExecutor executor = (JavascriptExecutor)driver;
			executor.executeScript("arguments[0].click();", modifyattribute);
			Log.message("modify attribute is clicked");
            SkySiteUtils.waitTill(6000);
			
		}
	/**Method to generate file name
	 * Scripted By:Tarak
	 * @return
	 */
	  public String Random_filename()
	    {
	           
	           String str="Filename";
	           Random r = new Random( System.currentTimeMillis() );
	           String abc = str+String.valueOf((10000 + r.nextInt(20000)));
	           return abc;
	           
	    }
	  /**Method to generate Title 
		 * Scripted By:Tarak
		 * @return
		 */
		  public String Random_title()
		    {
		           
		           String str="Title";
		           Random r = new Random( System.currentTimeMillis() );
		           String abc = str+String.valueOf((10000 + r.nextInt(20000)));
		           return abc;
		           
		    }
		  /**Method to generate description 
			 * Scripted By:Tarak
			 * @return
			 */
			  public String Random_description()
			    {
			           
			           String str="Description";
			           Random r = new Random( System.currentTimeMillis() );
			           String abc = str+String.valueOf((10000 + r.nextInt(20000)));
			           return abc;
			           
			    }
/**Method to generate Project No 
 * Scripted By:Tarak
	 * @return
 */
  public String Random_Projectno()
	  {
				          
				   String str="ProjectNo";
				   Random r = new Random( System.currentTimeMillis() );
				   String abc = str+String.valueOf((10000 + r.nextInt(20000)));
				   return abc;
				           
	  }
  /**Method to generate Project No 
   * Scripted By:Tarak
  	 * @return
   */
    public String Random_UpdateProjectno()
  	  {
  				          
  				   String str="UpdatedProjectNo";
  				   Random r = new Random( System.currentTimeMillis() );
  				   String abc = str+String.valueOf((10000 + r.nextInt(20000)));
  				   return abc;
  				           
  	  }
    /**Method to generate Project No 
     * Scripted By:Tarak
    	 * @return
     */
      public String Random_UpdateVendor()
    	  {
    				          
    				   String str="UpdatedVendorno";
    				   Random r = new Random( System.currentTimeMillis() );
    				   String abc = str+String.valueOf((10000 + r.nextInt(20000)));
    				   return abc;
    				           
    	  }
  /**Method to generate Project Name 
   * Scripted By:Tarak
  	 * @return
   */
    public String Random_Projectname()
  	  {
  				          
  				   String str="ProjectName";
  				   Random r = new Random( System.currentTimeMillis() );
  				   String abc = str+String.valueOf((10000 + r.nextInt(20000)));
  				   return abc;
  				           
  	  }
    /**Method to generate Invoice
     * Scripted By:Tarak
    	 * @return
     */
      public String Random_Invoice()
    	  {
    				          
    				   String str="Invoice";
    				   Random r = new Random( System.currentTimeMillis() );
    				   String abc = str+String.valueOf((10000 + r.nextInt(20000)));
    				   return abc;
    				           
    	  }
       

	@FindBy(css="#txt_fileName")
	@CacheLookup
	WebElement filename;
	@FindBy(css="#txt_title")
	@CacheLookup
	WebElement Title;
	@FindBy(css="#txt_desc")
	@CacheLookup
	WebElement Description;
	@FindBy(xpath="//input[@value='Update & close']")
	@CacheLookup
	WebElement updateclosebtn;
	
	/**Method to modify general attributes for file
	 * Scripted by Tarak
	 */
	public boolean modifyGeneralAttributes(String filerename,String title,String description) {
		SkySiteUtils.waitTill(3000);
		filename.clear();
		Log.message("filename is cleared");
		filename.sendKeys(filerename);
		Log.message("Rename for file entered is " +filerename);
		SkySiteUtils.waitTill(4000);
		Title.clear();
		Log.message("Title box is cleared");
		Title.sendKeys(title);
		Log.message("Title entered for file is " +title);
		SkySiteUtils.waitTill(4000);
		Description.clear();
		Log.message("Description is cleared");
		Description.sendKeys(description);
		Log.message("File description is " +description);
		SkySiteUtils.waitTill(4000);
		updateclosebtn.click();
		Log.message("update button is clicked");
		SkySiteUtils.waitTill(4000);
		int numberoffiles=driver.findElements(By.xpath(".//*[@id='divGrid']/div[2]/table/tbody/tr/td[1]/img")).size();
		Log.message("No of file present are " +numberoffiles);
		int i =0;
		for(i=1;i<=numberoffiles;i++) {
			String filename=driver.findElement(By.xpath("(.//*[@id='divGrid']/div[2]/table/tbody/tr/td[2])["+i+"]")).getText();
			SkySiteUtils.waitTill(2500);
			String Titleoffile=driver.findElement(By.xpath("(.//*[@id='divGrid']/div[2]/table/tbody/tr/td[6])["+i+"]")).getText();
			SkySiteUtils.waitTill(2500);
			String descriptionofile=driver.findElement(By.xpath("(.//*[@id='divGrid']/div[2]/table/tbody/tr/td[7])["+i+"]")).getText();
			SkySiteUtils.waitTill(2500);
		if(filename.contains(filerename)&&Titleoffile.contains(title)&&descriptionofile.contains(description))	
		{
			SkySiteUtils.waitTill(2500);
			Log.message("File name updated is  -->" +filename+ "  title updated is  --> " +Titleoffile+ "  description of file now is  --> " +descriptionofile);
			break;
			
		}
		}
		String newfilename=driver.findElement(By.xpath("(.//*[@id='divGrid']/div[2]/table/tbody/tr/td[2])["+i+"]")).getText();
		String newTitleoffile=driver.findElement(By.xpath("(.//*[@id='divGrid']/div[2]/table/tbody/tr/td[6])["+i+"]")).getText();
		String newdescriptionofile=driver.findElement(By.xpath("(.//*[@id='divGrid']/div[2]/table/tbody/tr/td[7])["+i+"]")).getText();
		SkySiteUtils.waitTill(3000);
		if(newfilename.contains(filerename)&&newTitleoffile.contains(title)&&newdescriptionofile.contains(description))	
		{
			Log.message("File general properties are updated");
			return true;
			
		}	
		else {
			Log.message("File general properties are not updated ");
			
			return false;
		}
			
	}
	@FindBy(css="#tbProjectKeyDate>a")
	@CacheLookup
	WebElement CoustomPropertiestab;
	@FindBy(xpath="//input[@placeholder='Project number']")
	@CacheLookup
	WebElement Projectnofield;
	@FindBy(xpath=".//*[@id='divCustomFiledData']/div/ul/li[4]/div/div[2]/div/div[2]/a/i")
	@CacheLookup
	WebElement Addicon;
	@FindBy(xpath=".//*[@id='divCustomFieldLookupGrid']/div[2]/table/tbody/tr[4]/td[1]/img")
	@CacheLookup
	WebElement selectVendoricon;
	@FindBy(xpath=".//*[@id='lookup_combo_10177904']/div/input[1]")
	@CacheLookup
	WebElement VendorInputBox;
	//for production
//	@FindBy(xpath="//*[@tabindex='3']")
	@FindBy(xpath="//*[@data-id='10178919']")
	//for production
	//@FindBy(xpath="//*[@data-id='10174639']")
	WebElement vendorinputboxupdate;
	//for production
	//@FindBy(xpath="//*[@data-id='10174639']")
		@FindBy(xpath="//*[@data-id='10178919']")
	WebElement vendorinputboxformodify;
	/**Method to modify custom properties:Vendor & Project No
	 * @throws AWTException 
	 * Scripted by : Tarak
	 */
	public boolean customPropertiesVendorAndPNo(String projectno) throws AWTException{
		String vendorname=PropertyReader.getProperty("Vendor");
		SkySiteUtils.waitTill(4000);
		CoustomPropertiestab.click();
		Log.message("Custome Properties tab clicked");
		SkySiteUtils.waitTill(4000);
		SkySiteUtils.waitForElement(driver,Projectnofield,40);
		Projectnofield.click();
		Log.message("Project no field is clicked");
		SkySiteUtils.waitTill(3000);
		//Projectnofield.clear();
		//Log.message("Project no field is cleared");
		//SkySiteUtils.waitTill(4000);
		
		Projectnofield.sendKeys(projectno);
		Log.message("Project number entered is " +projectno);
		SkySiteUtils.waitTill(3000);
		vendorinputboxformodify.click();
		Log.message("Vendor input box clicked");
	//	VendorInputBox.clear();
		//Log.message("Vendor box is cleard");
		SkySiteUtils.waitTill(4000);
		Actions act1=new Actions(driver);
		act1.sendKeys(vendorname).build().perform();
		//vendorinputboxupdate.sendKeys(vendorname);
		Log.message("Vendor name  is entered");
		SkySiteUtils.waitTill(2000);
		Robot rb= new Robot();
		rb.keyPress(KeyEvent.VK_ENTER);
		rb.keyRelease(KeyEvent.VK_ENTER);

		//selectVendoricon.click();
	
		SkySiteUtils.waitTill(4000);
	
 		updateclosebtn.click();
		Log.message("update button is clicked");
		SkySiteUtils.waitTill(4000);

		int numberoffiles=driver.findElements(By.xpath(".//*[@id='divGrid']/div[2]/table/tbody/tr/td[1]/img")).size();
		Log.message("No of file present are " +numberoffiles);
		int i =0;
		for(i=1;i<=numberoffiles;i++) {
			String Projectnopresent=driver.findElement(By.xpath("(.//*[@id='divGrid']/div[2]/table/tbody/tr/td[12])["+i+"]")).getText();
			String vendorpresent=driver.findElement(By.xpath("(.//*[@id='divGrid']/div[2]/table/tbody/tr/td[11])["+i+"]")).getText();
				
			if(Projectnopresent.contains(projectno)&&vendorpresent.contains(vendorname))	
			{
			
			    Log.message("File is present after searching with project number and vendor ");
				break;
				
			}
			}
		String Projectnopresent12=driver.findElement(By.xpath("(.//*[@id='divGrid']/div[2]/table/tbody/tr/td[12])["+i+"]")).getText();
		String vendorpresent12=driver.findElement(By.xpath("(.//*[@id='divGrid']/div[2]/table/tbody/tr/td[11])["+i+"]")).getText();
			
		if(Projectnopresent12.contains(projectno)&&vendorpresent12.contains(vendorname))	
		{
		
		    Log.message("File is present after searching with project number and vendor ");
		   return true;
			
		}
		else {
			
			
			Log.message("File is not present after search with project number and vendor");
			return false;
		}
	}
	@FindBy(css = "#lftpnlMore")
	@CacheLookup
	WebElement Moreoption;
	@FindBy(xpath=".//*[@onclick='javascript:RemoveFolder();']")
	@CacheLookup
	WebElement Removefolderoption;
	@FindBy(xpath = "//*[@id='button-1']")

	WebElement btnyes;
	/**Method to delete folder after execution
	 * 	Scripted by :Tarak
	 */
		public void deleteFolderFromCollection() {
				driver.switchTo().defaultContent();
				Log.message("Switching to default content");
				SkySiteUtils.waitTill(4000);
				driver.switchTo().frame(driver.findElement(By.id("myFrame")));
				Log.message("SWitched to frame");
				SkySiteUtils.waitTill(4000);
				// SkySiteUtils.waitForElement(driver, Moreoption, 50);

				Moreoption.click();
				SkySiteUtils.waitTill(4000);
				Log.message("More option is clicked");
				SkySiteUtils.waitTill(4000);
				Removefolderoption.click();
				Log.message("Remove folder button is clicked");
				SkySiteUtils.waitTill(10000);
				driver.switchTo().defaultContent();
				SkySiteUtils.waitTill(6000);
				btnyes.click();
				Log.message("yes button has been clicked.");
				
				SkySiteUtils.waitTill(6000);
		        Log.message("Folder has been deleted");
		}
		@FindBy(xpath=".//*[@id='lookup_combo_10176981']/div/input[1]")
		WebElement DocumentTypebox;
		@FindBy(xpath=".//*[@id='10176982']")
		WebElement invoicebox;
		@FindBy(xpath=".//*[@id='10177905']")
		WebElement Projectname;
		@FindBy(xpath=".//*[@id='10177907_true']")
		WebElement aprovalYes;
		@FindBy(xpath=".//*[@class='cflookup_combo']")
		WebElement documenttypesearchbox1;
		@FindBy(xpath="//*[@placeholder='Invoice #']")
		WebElement invoicebxoforupsate;
		@FindBy(xpath="//*[@placeholder='Project name']")
		WebElement projectnameboxupdate;
		/**Method to modify document type,invoice and project name
		 * scripted by Tarak
		 * @throws AWTException 
		 */
		public boolean modifyDocumentTypeInvoiceAndPName(String projectname,String invoice) throws AWTException {
			String documenttype=PropertyReader.getProperty("Documenttype");
			SkySiteUtils.waitTill(3000);
			CoustomPropertiestab.click();
			Log.message("Custome Properties tab clicked");
			SkySiteUtils.waitTill(4000);
			documenttypesearchbox1.click();
			Log.message("Document type box is clicked");
			SkySiteUtils.waitTill(4000);
	        Actions act=new Actions(driver);
	        act.sendKeys(documenttype).build().perform();
			//documenttypesearchbox1.sendKeys(documenttype);
			Log.message("Document type entered is " +documenttype);
			SkySiteUtils.waitTill(3000);
			Robot rb= new Robot();
			rb.keyPress(KeyEvent.VK_ENTER);
			rb.keyRelease(KeyEvent.VK_ENTER);
			SkySiteUtils.waitTill(4000);
			invoicebxoforupsate.click();
			Log.message("invoice box is clicked");
			invoicebxoforupsate.sendKeys(invoice);
			Log.message("Invoice is entered " +invoice);
			SkySiteUtils.waitTill(4000);
			projectnameboxupdate.click();
			Log.message("Project name box is clicked");
			projectnameboxupdate.sendKeys(projectname);
			Log.message("Project name entered is " +projectname);
			SkySiteUtils.waitTill(4000);
			updateclosebtn.click();
			Log.message("update button is clicked");
			SkySiteUtils.waitTill(4000);

			int numberoffiles=driver.findElements(By.xpath(".//*[@id='divGrid']/div[2]/table/tbody/tr/td[1]/img")).size();
			Log.message("No of file present are " +numberoffiles);
			int i =0;
			for(i=1;i<=numberoffiles;i++) {
				String Documentypeintable=driver.findElement(By.xpath("(.//*[@id='divGrid']/div[2]/table/tbody/tr/td[9])["+i+"]")).getText();
				SkySiteUtils.waitTill(2500);
				String invoiceintable=driver.findElement(By.xpath("(.//*[@id='divGrid']/div[2]/table/tbody/tr/td[10])["+i+"]")).getText();
				SkySiteUtils.waitTill(2500);
				String projectnameintable=driver.findElement(By.xpath("(.//*[@id='divGrid']/div[2]/table/tbody/tr/td[13])["+i+"]")).getText();
				SkySiteUtils.waitTill(2500);
				
			if(Documentypeintable.contains(documenttype)&&invoiceintable.contains(invoice))	
			{
				SkySiteUtils.waitTill(2500);
				Log.message("Document type updated is  -->" +Documentypeintable+ "  invoice updated is --> " +invoiceintable+ "Project name updated is " +projectnameintable+ "Approval has been seet to Yes");
				break;
				
			}
			}
			String NewDocumentypeintable=driver.findElement(By.xpath("(.//*[@id='divGrid']/div[2]/table/tbody/tr/td[9])["+i+"]")).getText();
			SkySiteUtils.waitTill(2500);
			String Newinvoiceintable=driver.findElement(By.xpath("(.//*[@id='divGrid']/div[2]/table/tbody/tr/td[10])["+i+"]")).getText();
			SkySiteUtils.waitTill(2500);
			String Newprojectnameintable=driver.findElement(By.xpath("(.//*[@id='divGrid']/div[2]/table/tbody/tr/td[13])["+i+"]")).getText();
			SkySiteUtils.waitTill(2500);
		
			if(NewDocumentypeintable.contains(documenttype)&&Newinvoiceintable.contains(invoice)&&Newprojectnameintable.contains(projectname))	
			{
				SkySiteUtils.waitTill(2500);
				Log.message("Document & Invoice & project name is updated ");
			    return true;
				
			}
			else {
				
				Log.message("Document & Invoice & project name is not  updated ");
				
				return false;
			}
			
	}
		@FindBy(xpath=".//*[@id='div_otherInfo']/div/div/div[3]/input[2]")
		@CacheLookup
		WebElement closebtn;
			@FindBy(id="Button6")
		// @FindBy(xpath=".//*[@id='btnAdvFileSearch']")
		WebElement advancedsearch;
		 @FindBy(xpath="//*[contains(text(),'Reset')]")
		// @FindBy(xpath=".//*[@id='divAdvSearchActionForCallingPage']/button[2]")
		WebElement resetbtn;
		@FindBy(xpath="//*[contains(text(),'With Document Type')]")
		
		WebElement checkbox;
		@FindBy(xpath="//input[@class='dhxcombo_input']")
		@CacheLookup
		WebElement documenttypesearchbox;  
		@FindBy(id="btnAdvSearch")
	
		WebElement Searchbtn;
		 /**  Method to search with document type
		 * Scriptedby:Tarak
		 * @throws AWTException 
		 */
		public boolean DocumentTypeSearch() throws AWTException {
			
			closebtn.click();
			Log.message("Close button is clicked");
			SkySiteUtils.waitForElement(driver, advancedsearch,50);
			advancedsearch.click();
			SkySiteUtils.waitTill(7000);
		    Log.message("Advanced search option is clikced");
			SkySiteUtils.waitTill(7000);
			SkySiteUtils.waitForElement(driver, resetbtn, 50);
			resetbtn.click();
			Log.message("reset button is clicked");
			Actions actclick=new Actions(driver);
		    SkySiteUtils.waitForElement(driver, checkbox, 40);
			actclick.moveToElement(checkbox).click(checkbox).build().perform();
			Log.message("checkbox is clicked");
			String documenttypesearch=PropertyReader.getProperty("DocumentTypeSearch");
		    SkySiteUtils.waitForElement(driver,documenttypesearchbox, 40);
		    documenttypesearchbox.sendKeys(documenttypesearch);
		    Log.message("Document type searched " +documenttypesearch);
		    SkySiteUtils.waitTill(3000);
			Robot rb= new Robot();
			rb.keyPress(KeyEvent.VK_ENTER);
			rb.keyRelease(KeyEvent.VK_ENTER);
			SkySiteUtils.waitTill(8000);
			Searchbtn.click();
			Log.message("search button is clicked");
			SkySiteUtils.waitTill(4000);
			SkySiteUtils.waitForElement(driver,advancedsearch, 40);
			int numberoffiles=driver.findElements(By.xpath(".//*[@id='divGrid']/div[2]/table/tbody/tr/td[1]/img")).size();
			Log.message("No of file present are " +numberoffiles);
			int i =0;
			for(i=1;i<=numberoffiles;i++) {
			String documenttypepresent=driver.findElement(By.xpath("(.//*[@id='divGrid']/div[2]/table/tbody/tr/td[9])["+i+"]")).getText();
				
			if(documenttypepresent.contains(documenttypesearch)&&numberoffiles==1)	
			{
			
			    Log.message("Document type searched is present ");
				break;
				
			}
			}
			String documenttypepresentinlist=driver.findElement(By.xpath("(.//*[@id='divGrid']/div[2]/table/tbody/tr/td[9])["+i+"]")).getText();
			
			if(documenttypepresentinlist.contains(documenttypesearch)&&numberoffiles==1)	
			{
			
			    Log.message("Search for document type is validated ");
				return true;
				
			}
			else {
				Log.message("document type searched is not present");
				return false;
			}	
			
			
	}
		@FindBy(xpath="//*[contains(text(),'With Invoice #')]")
		@CacheLookup
		WebElement checkboxforinvoice;
		@FindBy(xpath=".//*[@placeholder='Invoice #']")
		@CacheLookup
		WebElement invoicesearchbox;
		@FindBy(xpath="//select[@id='sddlcond_Search_10177040']")
		@CacheLookup
		WebElement criteriadropdown;
		/**Method to search with invoice
		 * scripted by :Tarak
		 * @throws AWTException 
		 */
		public boolean searchInvoice() throws AWTException {
			
			closebtn.click();
			Log.message("Close button is clicked");
			SkySiteUtils.waitForElement(driver, advancedsearch,50);
	        Actions actclick=new Actions(driver);
		    actclick.moveToElement(advancedsearch).click(advancedsearch).build().perform();
		    Log.message("Advanced search option is clikced");
			SkySiteUtils.waitForElement(driver, resetbtn, 40);
			resetbtn.click();
			Log.message("reset button is clicked");
		    SkySiteUtils.waitForElement(driver, checkboxforinvoice, 40);
			actclick.moveToElement(checkboxforinvoice).click(checkboxforinvoice).build().perform();
			Log.message("checkbox is clicked");
			SkySiteUtils.waitTill(5000);
           // Select sc=new Select(criteriadropdown);
          // sc.selectByValue("1");
            SkySiteUtils.waitTill(3000);
			String invoicesearch=PropertyReader.getProperty("Invoice");
		    SkySiteUtils.waitForElement(driver,invoicesearchbox, 40);
		    invoicesearchbox.sendKeys(invoicesearch);
		    Log.message("invoice searched is " +invoicesearch);
		    SkySiteUtils.waitTill(3000);
			Robot rb= new Robot();
			rb.keyPress(KeyEvent.VK_ENTER);
			rb.keyRelease(KeyEvent.VK_ENTER);
			SkySiteUtils.waitTill(5000);
			Searchbtn.click();
			Log.message("search button is clicked");
			SkySiteUtils.waitTill(4000);
			SkySiteUtils.waitForElement(driver,advancedsearch, 40);
			int numberoffiles=driver.findElements(By.xpath(".//*[@id='divGrid']/div[2]/table/tbody/tr/td[1]/img")).size();
			Log.message("No of file present are " +numberoffiles);
			int i =0;
			for(i=1;i<=numberoffiles;i++) {
			String invoicepresent=driver.findElement(By.xpath("(.//*[@id='divGrid']/div[2]/table/tbody/tr/td[10])["+i+"]")).getText();
				
			if(invoicepresent.contains(invoicesearch)&&numberoffiles==1)	
			{
			
			    Log.message("File is present after searching with invoice attribute ");
				break;
				
			}
			}
			String invoicepresentinlist=driver.findElement(By.xpath("(.//*[@id='divGrid']/div[2]/table/tbody/tr/td[10])["+i+"]")).getText();
			
			if(invoicepresentinlist.contains(invoicesearch)&&numberoffiles==1)	
			{
			
			    Log.message("Search with invoice is validated ");
				return true;
				
			}
			else {
				Log.message("search with invoice  is not validated");
				return false;
			}	
	}
		
		@FindBy(xpath="//input[contains(text(),'With project number')]")
		//@CacheLookup
		WebElement checkboxforprojectnumber;
		@FindBy(xpath="//input[@placeholder='project number']")
		@CacheLookup
		WebElement projectsearchbox;
		
		/**Method to search with Project Number
		 * scripted by:Tarak
		 * @throws AWTException 
		 */
		public boolean searchWithProjectNumber() throws AWTException {
			
			closebtn.click();
			Log.message("Close button is clicked");
			SkySiteUtils.waitForElement(driver, advancedsearch,50);
	        Actions actclick12=new Actions(driver);
		    actclick12.moveToElement(advancedsearch).click(advancedsearch).build().perform();
		    Log.message("Advanced search option is clicked");
			SkySiteUtils.waitForElement(driver, resetbtn, 40);
			resetbtn.click();
			Log.message("reset button is clicked");
			SkySiteUtils.waitTill(2000);
			String Projectnosearch=PropertyReader.getProperty("Project_Number");
		    WebElement Projectnobox=driver.findElement(By.xpath("//input[@placeholder='Project number']"));
		    Projectnobox.click();
		    Log.message("poject search box is clicked");
		    SkySiteUtils.waitTill(4000);
		    Projectnobox.sendKeys(Projectnosearch);
		    Log.message("Project number entered is " +Projectnosearch);
		    SkySiteUtils.waitTill(5000);
		    Searchbtn.click();
			Log.message("search button is clicked");
			SkySiteUtils.waitTill(4000);
			SkySiteUtils.waitForElement(driver,advancedsearch, 40);
			int numberoffiles=driver.findElements(By.xpath(".//*[@id='divGrid']/div[2]/table/tbody/tr/td[1]/img")).size();
			Log.message("No of file present are " +numberoffiles);
			String filenamesearched=PropertyReader.getProperty("Searchfilename");
			
			int i =0;
			for(i=1;i<=numberoffiles;i++) {
					
			String fileinlist =driver.findElement(By.xpath("(.//*[@id='divGrid']/div[2]/table/tbody/tr/td[2])["+i+"]")).getText();
					
			if(fileinlist.contains(filenamesearched)&&numberoffiles==1)	
			{
			
				  Log.message("File is present after searching with Project number");
					break;
					
		     }
		}
			String filepresentinlist=driver.findElement(By.xpath("(.//*[@id='divGrid']/div[2]/table/tbody/tr/td[2])["+i+"]")).getText();
				
			if(filepresentinlist.contains(filenamesearched)&&numberoffiles==1)	
			{
				
				    Log.message("Search with Project number is validated !!!!!");
					return true;
					
			}
		else {
					Log.message("Search with Project number  is not validated");
					return false;
			}	
		}
		
		
		
		@FindBy(xpath="//input[contains(text(),'With vendor')]")
		@CacheLookup
		WebElement checkboxforvendor;
		@FindBy(xpath =".//*[@id='lookup_combo_Search_10177908']/div/input[1]")
		@CacheLookup
		WebElement vendorsearchbox;
		/**Search with Vendor
		 * 
		 */
		public boolean searchWithVendor() throws AWTException {

			closebtn.click();
			Log.message("Close button is clicked");
			SkySiteUtils.waitForElement(driver, advancedsearch,50);
	        Actions actclick12=new Actions(driver);
		    actclick12.moveToElement(advancedsearch).click(advancedsearch).build().perform();
		    Log.message("Advanced search option is clikced");
			SkySiteUtils.waitForElement(driver, resetbtn, 40);
			resetbtn.click();
		
			Log.message("reset button is clicked");
			SkySiteUtils.waitTill(3000);
			
		
			SkySiteUtils.waitTill(5000);
			String Vendorforsearch=PropertyReader.getProperty("VendorforSearch");
			//WebElement vendorsearchbox=driver.findElement(By.xpath(".//*[@id='lookup_combo_Search_10174639']/div/input[1]"));
			//WebElement vendorsearchbox=driver.findElement(By.xpath(".//*[@data-id='10174639']"));
			WebElement vendorsearchbox=driver.findElement(By.xpath(".//*[@data-id='10178919']"));
			SkySiteUtils.waitForElement(driver,vendorsearchbox, 40);
		    SkySiteUtils.waitTill(4000);
		    vendorsearchbox.click();
		    Log.message("Search box is clicked");
		    SkySiteUtils.waitTill(6000);
		    Actions act2=new Actions(driver);
		    act2.sendKeys(Vendorforsearch).build().perform();
		    SkySiteUtils.waitTill(4000);
	
		    Log.message("Vendor searched is " +Vendorforsearch);
		    SkySiteUtils.waitTill(6000);
		   
			Robot rb= new Robot();
			rb.keyPress(KeyEvent.VK_DOWN);
			rb.keyRelease(KeyEvent.VK_DOWN);
			rb.keyPress(KeyEvent.VK_ENTER);
			rb.keyRelease(KeyEvent.VK_ENTER);
			Log.message("Enter is pressed");
			SkySiteUtils.waitTill(5000);
			Searchbtn.click();
			Log.message("search button is clicked");
			SkySiteUtils.waitTill(4000);
			SkySiteUtils.waitForElement(driver,advancedsearch, 40);
			String filenamesearched=PropertyReader.getProperty("Searchfilename");
			int numberoffiles=driver.findElements(By.xpath(".//*[@id='divGrid']/div[2]/table/tbody/tr/td[1]/img")).size();
			Log.message("No of file present are " +numberoffiles);
			int i =0;
			for(i=1;i<=numberoffiles;i++) {
					
			String fileinlist =driver.findElement(By.xpath("(.//*[@id='divGrid']/div[2]/table/tbody/tr/td[2])["+i+"]")).getText();
					
			if(fileinlist.contains(filenamesearched)&&numberoffiles==1)	
			{
			
				  Log.message("File is present after searching with Vendor");
					break;
					
		     }
		}
			String filepresentinlist=driver.findElement(By.xpath("(.//*[@id='divGrid']/div[2]/table/tbody/tr/td[2])["+i+"]")).getText();
				
			if(filepresentinlist.contains(filenamesearched)&&numberoffiles==1)	
			{
				
				    Log.message("Search with Vendor is validated !!!!!");
					return true;
					
			}
		else {
					Log.message("Search with Vendor  is not validated");
					return false;
			}	
		
		}
		/**Method to search for Projectname
		 * Scripted by:Tarak
		 * @throws AWTException 
		 */
	public boolean searchWithProjectName() throws AWTException {
		closebtn.click();
		Log.message("Close button is clicked");
		SkySiteUtils.waitForElement(driver, advancedsearch,50);
        Actions actclick=new Actions(driver);
	    actclick.moveToElement(advancedsearch).click(advancedsearch).build().perform();
	    Log.message("Advanced search option is clikced");
		SkySiteUtils.waitForElement(driver, resetbtn, 40);
		resetbtn.click();
		Log.message("reset button is clicked");
	    WebElement projectnamebox=driver.findElement(By.xpath("//input[@placeholder='Project name']"));
	    SkySiteUtils.waitTill(3000);
		String Projectnameforsearch=PropertyReader.getProperty("Project_Name");
	    SkySiteUtils.waitForElement(driver,projectnamebox, 40);
	    projectnamebox.click();
	    Log.message("Project name box is clicked");
	    projectnamebox.sendKeys(Projectnameforsearch);
	    Log.message("Project name searched is " +Projectnameforsearch);
	    SkySiteUtils.waitTill(5000);
	    Searchbtn.click();
		Log.message("search button is clicked");
		SkySiteUtils.waitTill(4000);
		SkySiteUtils.waitForElement(driver,advancedsearch, 40);
		int numberoffiles=driver.findElements(By.xpath(".//*[@id='divGrid']/div[2]/table/tbody/tr/td[1]/img")).size();
		Log.message("No of file present are " +numberoffiles);
		String filenamesearched=PropertyReader.getProperty("Searchfilename");
		
		int i =0;
		for(i=1;i<=numberoffiles;i++) {
				
		String fileinlist =driver.findElement(By.xpath("(.//*[@id='divGrid']/div[2]/table/tbody/tr/td[2])["+i+"]")).getText();
				
		if(fileinlist.contains(filenamesearched)&&numberoffiles==1)	
		{
		
			  Log.message("File is present after searching with Project name");
				break;
				
	     }
	}
		String filepresentinlist=driver.findElement(By.xpath("(.//*[@id='divGrid']/div[2]/table/tbody/tr/td[2])["+i+"]")).getText();
			
		if(filepresentinlist.contains(filenamesearched)&&numberoffiles==1)	
		{
			
			    Log.message("Search with Project name is validated !!!!!");
				return true;
				
		}
	else {
				Log.message("Search with Project name  is not validated");
				return false;
		}	
		
	}
	@FindBy(xpath=".//*[@id='txtContentSearch']")
	@CacheLookup
	WebElement keywordcontentsearch;
	/**Method for search with Keyword
	 * 
	 */
	public boolean searchWithKeyword() {
		closebtn.click();
		Log.message("Close button is clicked");
		SkySiteUtils.waitForElement(driver, advancedsearch,50);
        Actions actclick=new Actions(driver);
	    actclick.moveToElement(advancedsearch).click(advancedsearch).build().perform();
	    Log.message("Advanced search option is clikced");
		SkySiteUtils.waitForElement(driver, resetbtn, 40);
		resetbtn.click();
		Log.message("reset button is clicked");
		SkySiteUtils.waitTill(3000);
		String keywordforsearch=PropertyReader.getProperty("Keywordforsearch");
		keywordcontentsearch.sendKeys(keywordforsearch);
		Log.message("Keyword searched is " +keywordforsearch);
		 SkySiteUtils.waitTill(5000);
		 Searchbtn.click();
	    Log.message("search button is clicked");
	    SkySiteUtils.waitTill(4000);
		SkySiteUtils.waitForElement(driver,advancedsearch, 40);
		int numberoffiles=driver.findElements(By.xpath(".//*[@id='divGrid']/div[2]/table/tbody/tr/td[1]/img")).size();
		Log.message("No of file present are " +numberoffiles);
		String filenamesearched=PropertyReader.getProperty("Filenameforkeywordsearch");
		Log.message("File name to be searched is " +filenamesearched);
		int i =0;
		for(i=1;i<=numberoffiles;i++) {
				
		String fileinlist =driver.findElement(By.xpath("(.//*[@id='divGrid']/div[2]/table/tbody/tr/td[2])["+i+"]")).getText();
				
		if(fileinlist.contains(filenamesearched)&&numberoffiles==1)	
		{
		
			  Log.message("File is present after searching with Keyword");
				break;
				
	     }
	}
		String filepresentinlist=driver.findElement(By.xpath("(.//*[@id='divGrid']/div[2]/table/tbody/tr/td[2])["+i+"]")).getText();
			
		if(filepresentinlist.contains(filenamesearched)&&numberoffiles==1)	
		{
			
			    Log.message("Search with keyword is validated !!!!!");
				return true;
				
		}
	else {
				Log.message("Search with  keyword  is not validated");
				return false;
		}	
	
   }
	@FindBy(css = "#btnUploadFile")

	WebElement btnUploadFile;

//	@FindBy(xpath = ".//*[@id='fine-uploader']/div/div[2]/div/div[2]/input")
	@FindBy(xpath = ".//*[@id='btnSelectFiles']")
	WebElement btnselctFile;
	
	@FindBy(css="#btnFileAttrLink")
	WebElement addattributelink;
	@FindBy(xpath=".//*[@id='btnSaveNClose']")
	WebElement savebtn;
	@FindBy(xpath = ".//*[@id='chkAllProjectdocs']")

	WebElement checkboxforfileselect;
	/**Method to upload file and add attribute
	 * 
	 */
	
	public void UploadFileAddAttribute(String FolderPath,String tempfilepath,String invoice) throws AWTException, IOException {
		// boolean result1=true;
		driver.switchTo().defaultContent();
		Log.message("Swtiched to default content");
		SkySiteUtils.waitTill(4000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		SkySiteUtils.waitTill(5000);
		btnUploadFile.click(); // Adding on New Folder
		Log.message("upload button has been clicked");
		SkySiteUtils.waitTill(8000);
		String parentHandle = driver.getWindowHandle();
		Log.message(parentHandle);
		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
			SkySiteUtils.waitTill(5000);// switch focus of WebDriver to the next found window handle (that's your newly
										// opened window)
		}
		SkySiteUtils.waitTill(10000);
		btnselctFile.click();
		Log.message("select files has been clicked");
		SkySiteUtils.waitTill(5000);
		//Writing File names into a text file for using in AutoIT Script
		BufferedWriter output;			
  		randomFileName rn=new randomFileName();
  		String tmpFileName=tempfilepath+rn.nextFileName()+".txt";			
  		output = new BufferedWriter(new FileWriter(tmpFileName,true));	
  				
  		String expFilename=null;
  		File[] files = new File(FolderPath).listFiles();
  				
  		for(File file : files)
  		{
  			if(file.isFile()) 
  			{
  				expFilename=file.getName();//Getting File Names into a variable
  				Log.message("Expected File name is:"+expFilename);	
  				output.append('"' + expFilename + '"');
  				output.append(" ");
  				SkySiteUtils.waitTill(5000);	
  			}	
  		}	
  		output.flush();
  		output.close();	
  				
  		String AutoItexe_Path = PropertyReader.getProperty("AutoItexe1");
  		//Executing .exe autoIt file		 
  		Runtime.getRuntime().exec(AutoItexe_Path+" "+ FolderPath+" "+tmpFileName );			
  		Log.message("AutoIT Script Executed!!");				
  		SkySiteUtils.waitTill(20000);
  				
  		try
  		{
  			File file = new File(tmpFileName);
  			if(file.delete())
  			{
  				Log.message(file.getName() + " is deleted!");
  			}	
  			else
  			{
  				Log.message("Delete operation is failed.");
  			}
  		}
  		catch(Exception e)
  		{			
  			Log.message("Exception occured!!!"+e);
  		}			
  		SkySiteUtils.waitTill(8000);
  		addattributelink.click();
  		Log.message("Add attribute link clicked");
  		SkySiteUtils.waitTill(5000);
  		driver.switchTo().frame(driver.findElement(By.id("IframeSetBatchAttribute")));
  		Log.message("switch to frame");
  		checkboxforfileselect.click();
  		Log.message("checkbox clicked");
  		SkySiteUtils.waitTill(5000);
  	//	String filetoselect=PropertyReader.getProperty("Filenameforkeywordsearch");
  	//	String filetoselect=PropertyReader.getProperty("Filenametoaddattribute");
  	//	List<WebElement> filelist=driver.findElements(By.xpath("//*[@id='divAttributeGrid']/div[2]/table/tbody/tr/td"));
  	//	for (WebElement var:filelist)
  		///	if(var.getText().contains(filetoselect))
  		        //       var.click();
  		driver.findElement(By.xpath(".//*[@id='divAttributeGrid']/div[2]/table/tbody/tr[3]/td")).click();
  		Log.message("file is selected");
  		SkySiteUtils.waitTill(6000);

  		WebElement invoicebox12=driver.findElement(By.xpath("//input[@placeholder='Invoice #']"));
        Actions act4=new Actions(driver);
        act4.moveToElement(invoicebox12).click().build().perform();;
  		Log.message("Invoice box is clicked");
  		SkySiteUtils.waitTill(2000);
  		invoicebox12.sendKeys(invoice);
		Log.message("Invoice is entered " +invoice);
  		SkySiteUtils.waitTill(2000);
  		savebtn.click();
  		Log.message("save button is clicked");
  	
		SkySiteUtils.waitTill(30000);
		driver.switchTo().window(parentHandle);// Switch back to folder page
		SkySiteUtils.waitTill(5000);
		driver.switchTo().defaultContent();
		
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		SkySiteUtils.waitTill(8000);

	}
	/**Method to search with invoice for the updated file and invoice attribute
	 * scripted by :Tarak
	 * @throws AWTException 
	 */
	public boolean searchInvoiceonupdatedfile(String invoice) throws AWTException {
		
		closebtn.click();
		Log.message("Close button is clicked");
		SkySiteUtils.waitForElement(driver, advancedsearch,50);
        Actions actclick=new Actions(driver);
	    actclick.moveToElement(advancedsearch).click(advancedsearch).build().perform();
	    Log.message("Advanced search option is clikced");
		SkySiteUtils.waitForElement(driver, resetbtn, 40);
		resetbtn.click();
		Log.message("reset button is clicked");
	    SkySiteUtils.waitForElement(driver, checkboxforinvoice, 40);
		actclick.moveToElement(checkboxforinvoice).click(checkboxforinvoice).build().perform();
		Log.message("checkbox is clicked");
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver,invoicesearchbox, 40);
	    invoicesearchbox.sendKeys(invoice);
	    Log.message("invoice searched is " +invoice);
	    SkySiteUtils.waitTill(3000);
		Robot rb= new Robot();
		rb.keyPress(KeyEvent.VK_ENTER);
		rb.keyRelease(KeyEvent.VK_ENTER);
		SkySiteUtils.waitTill(5000);
		Searchbtn.click();
		Log.message("search button is clicked");
		SkySiteUtils.waitTill(4000);
		SkySiteUtils.waitForElement(driver,advancedsearch, 40);
		int numberoffiles=driver.findElements(By.xpath(".//*[@id='divGrid']/div[2]/table/tbody/tr/td[1]/img")).size();
		Log.message("No of file present are " +numberoffiles);
		int i =0;
		for(i=1;i<=numberoffiles;i++) {
		String invoicepresent=driver.findElement(By.xpath("(.//*[@id='divGrid']/div[2]/table/tbody/tr/td[10])["+i+"]")).getText();
			
		if(invoicepresent.contains(invoice)&&numberoffiles==1)	
		{
		
		    Log.message("File is present after searching with invoice attribute ");
			break;
			
		}
		}
		String invoicepresentinlist=driver.findElement(By.xpath("(.//*[@id='divGrid']/div[2]/table/tbody/tr/td[10])["+i+"]")).getText();
		
		if(invoicepresentinlist.contains(invoice)&&numberoffiles==1)	
		{
		
		    Log.message("Search with invoice is validated ");
			return true;
			
		}
		else {
			Log.message("search with invoice  is not validated");
			return false;
		}	
}
	/**Method to upload file and add custom  attribute
	 * 
	 */
	
	public void UploadFileAddCustomAttribute(String FolderPath,String tempfilepath) throws AWTException, IOException {
		// boolean result1=true;
		driver.switchTo().defaultContent();
		Log.message("Swtiched to default content");
		SkySiteUtils.waitTill(4000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		SkySiteUtils.waitTill(5000);
		btnUploadFile.click(); // Adding on New Folder
		Log.message("upload button has been clicked");
		SkySiteUtils.waitTill(8000);
		String parentHandle = driver.getWindowHandle();
		Log.message(parentHandle);
		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
			SkySiteUtils.waitTill(5000);// switch focus of WebDriver to the next found window handle (that's your newly
										// opened window)
		}
		SkySiteUtils.waitTill(10000);
		btnselctFile.click();
		Log.message("select files has been clicked");
		SkySiteUtils.waitTill(10000);
		//Writing File names into a text file for using in AutoIT Script
		BufferedWriter output;			
  		randomFileName rn=new randomFileName();
  		String tmpFileName=tempfilepath+rn.nextFileName()+".txt";			
  		output = new BufferedWriter(new FileWriter(tmpFileName,true));	
  				
  		String expFilename=null;
  		File[] files = new File(FolderPath).listFiles();
  				
  		for(File file : files)
  		{
  			if(file.isFile()) 
  			{
  				expFilename=file.getName();//Getting File Names into a variable
  				Log.message("Expected File name is:"+expFilename);	
  				output.append('"' + expFilename + '"');
  				output.append(" ");
  				SkySiteUtils.waitTill(5000);	
  			}	
  		}	
  		output.flush();
  		output.close();	
  				
  		String AutoItexe_Path = PropertyReader.getProperty("AutoItexe1");
  		//Executing .exe autoIt file		 
  		Runtime.getRuntime().exec(AutoItexe_Path+" "+ FolderPath+" "+tmpFileName );			
  		Log.message("AutoIT Script Executed!!");				
  		SkySiteUtils.waitTill(20000);
  				
  		try
  		{
  			File file = new File(tmpFileName);
  			if(file.delete())
  			{
  				Log.message(file.getName() + " is deleted!");
  			}	
  			else
  			{
  				Log.message("Delete operation is failed.");
  			}
  		}
  		catch(Exception e)
  		{			
  			Log.message("Exception occured!!!"+e);
  		}			
  		SkySiteUtils.waitTill(8000);
  		addattributelink.click();
  		Log.message("Add attribute link clicked");
  		SkySiteUtils.waitTill(5000);
  		driver.switchTo().frame(driver.findElement(By.id("IframeSetBatchAttribute")));
  		Log.message("switch to frame");
  		checkboxforfileselect.click();
  		Log.message("checkbox clicked");
  		SkySiteUtils.waitTill(6000);
  	//	String filetoselect=PropertyReader.getProperty("Filenameforkeywordsearch");
  	//	String filetoselect=PropertyReader.getProperty("Filenametoaddattribute");
  	//	List<WebElement> filelist=driver.findElements(By.xpath("//*[@id='divAttributeGrid']/div[2]/table/tbody/tr/td"));
  	//	for (WebElement var:filelist)
  		//	if(var.getText().contains(filetoselect))
  		              // var.click();
		driver.findElement(By.xpath(".//*[@id='divAttributeGrid']/div[2]/table/tbody/tr[3]/td")).click();
  		Log.message("file is selected");
  		SkySiteUtils.waitTill(6000);

  	//	Log.message("file is selected");
  	//	SkySiteUtils.waitTill(2000);
        String vendordata=PropertyReader.getProperty("VendorforSearch");
        //for Productions
   // WebElement vendorsearchbox=driver.findElement(By.xpath(".//*[@data-id='10174639']"));
       WebElement vendorsearchbox=driver.findElement(By.xpath(".//*[@data-id='10178919']"));
        vendorsearchbox.click();
  		Log.message("Vendor box is clicked");
  		SkySiteUtils.waitTill(2000);
  		Actions act6=new Actions(driver);
  		act6.sendKeys(vendordata).build().perform();
  		//vendorsearchbox.sendKeys(vendordata);
		Log.message("Vendor  is entered " +vendordata);
  		SkySiteUtils.waitTill(2000);
  		Robot rb=new Robot();
  		rb.keyPress(KeyEvent.VK_ENTER);
  		rb.keyRelease(KeyEvent.VK_ENTER);
  		
  		savebtn.click();
  		Log.message("save button is clicked");
  	
		SkySiteUtils.waitTill(30000);
		driver.switchTo().window(parentHandle);// Switch back to folder page
		SkySiteUtils.waitTill(5000);
		driver.switchTo().defaultContent();
		
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		SkySiteUtils.waitTill(8000);

	}
	
  	/**Method search with All attribute using And operator
  	 * @throws AWTException 
  	 * 
  	 */
    public boolean searchWithAllAttributeAndOperator() throws AWTException {
    	closebtn.click();
		Log.message("Close button is clicked");
		SkySiteUtils.waitForElement(driver, advancedsearch,50);
        Actions actclick=new Actions(driver);
	    actclick.moveToElement(advancedsearch).click(advancedsearch).build().perform();
	    Log.message("Advanced search option is clikced");
		SkySiteUtils.waitForElement(driver, resetbtn, 40);
		resetbtn.click();
		Log.message("reset button is clicked");
		SkySiteUtils.waitForElement(driver, checkbox, 40);
	    actclick.moveToElement(checkbox).click(checkbox).build().perform();
		Log.message("checkbox is clicked");
		String documenttypesearch=PropertyReader.getProperty("DocumentTypeSearch");
		SkySiteUtils.waitForElement(driver,documenttypesearchbox, 40);
		documenttypesearchbox.sendKeys(documenttypesearch);
		Log.message("Document type searched " +documenttypesearch);
		SkySiteUtils.waitTill(3000);
	    Robot rb= new Robot();
		rb.keyPress(KeyEvent.VK_ENTER);
		rb.keyRelease(KeyEvent.VK_ENTER);
		SkySiteUtils.waitTill(5000);
	    SkySiteUtils.waitForElement(driver, checkboxforinvoice, 40);
		actclick.moveToElement(checkboxforinvoice).click(checkboxforinvoice).build().perform();
		Log.message("checkbox is clicked");
		SkySiteUtils.waitTill(5000);
       // Select sc=new Select(criteriadropdown);
      // sc.selectByValue("1");
        SkySiteUtils.waitTill(3000);
		String invoicesearch=PropertyReader.getProperty("Invoice");
	    SkySiteUtils.waitForElement(driver,invoicesearchbox, 40);
	    invoicesearchbox.sendKeys(invoicesearch);
	    Log.message("invoice searched is " +invoicesearch);
	    SkySiteUtils.waitTill(3000);
		Robot rb1= new Robot();
		rb1.keyPress(KeyEvent.VK_ENTER);
		rb1.keyRelease(KeyEvent.VK_ENTER);
		SkySiteUtils.waitTill(5000);
		String Vendorforsearch=PropertyReader.getProperty("VendorforSearch");
		//WebElement vendorsearchbox=driver.findElement(By.xpath(".//*[@id='lookup_combo_Search_10174639']/div/input[1]"));
		 //for production
	//	WebElement vendorsearchbox=driver.findElement(By.xpath(".//*[@data-id='10174639']"));
		WebElement vendorsearchbox=driver.findElement(By.xpath(".//*[@data-id='10178919']"));
		SkySiteUtils.waitForElement(driver,vendorsearchbox, 40);
		//SkySiteUtils.waitForElement(driver,vendorsearchbox, 40);
	    SkySiteUtils.waitTill(4000);
	    vendorsearchbox.click();
	    Log.message("Search box is clicked");
	    SkySiteUtils.waitTill(6000);
	    Actions act3=new Actions(driver);
	    act3.sendKeys(Vendorforsearch).build().perform();
	  //  vendorsearchbox.sendKeys(Vendorforsearch);
	    SkySiteUtils.waitTill(4000);

	    Log.message("Vendor searched is " +Vendorforsearch);
	    SkySiteUtils.waitTill(6000);
	   
		Robot rb2= new Robot();
		rb2.keyPress(KeyEvent.VK_DOWN);
		rb2.keyRelease(KeyEvent.VK_DOWN);
		rb2.keyPress(KeyEvent.VK_ENTER);
		rb2.keyRelease(KeyEvent.VK_ENTER);
		Log.message("Enter is pressed");
		SkySiteUtils.waitTill(5000);
		String Projectnosearch=PropertyReader.getProperty("Project_Number");
	    WebElement Projectnobox=driver.findElement(By.xpath("//input[@placeholder='Project number']"));
	    Actions act4=new Actions(driver);
	    act4.moveToElement(Projectnobox).click(Projectnobox).build().perform();
	    Log.message("poject search box is clicked");
	    SkySiteUtils.waitTill(4000);
	    Projectnobox.sendKeys(Projectnosearch);
	    Log.message("Project number entered is " +Projectnosearch);
	    SkySiteUtils.waitTill(5000);
	    WebElement projectnamebox=driver.findElement(By.xpath("//input[@placeholder='Project name']"));
	    SkySiteUtils.waitTill(3000);
		String Projectnameforsearch=PropertyReader.getProperty("Project_Name");
	    SkySiteUtils.waitForElement(driver,projectnamebox, 40);
	    projectnamebox.click();
	    Log.message("Project name box is clicked");
	    projectnamebox.sendKeys(Projectnameforsearch);
	    Log.message("Project name searched is " +Projectnameforsearch);
	    SkySiteUtils.waitTill(5000);
		Searchbtn.click();
		Log.message("search button is clicked");
		SkySiteUtils.waitTill(4000);
		SkySiteUtils.waitForElement(driver,advancedsearch, 40);
		String filenamesearched=PropertyReader.getProperty("filesearchwithallattribute");
		int numberoffiles=driver.findElements(By.xpath(".//*[@id='divGrid']/div[2]/table/tbody/tr/td[1]/img")).size();
		Log.message("No of file present are " +numberoffiles);
		int i =0;
		for(i=1;i<=numberoffiles;i++) {
			String fileinlist =driver.findElement(By.xpath("(.//*[@id='divGrid']/div[2]/table/tbody/tr/td[2])["+i+"]")).getText();
			
			if(fileinlist.contains(filenamesearched)&&numberoffiles==1)	
			{
			
				  Log.message("File is present after searching with All Attribute using & operator");
					break;
					
		     }
		}
			String filepresentinlist=driver.findElement(By.xpath("(.//*[@id='divGrid']/div[2]/table/tbody/tr/td[2])["+i+"]")).getText();
			SkySiteUtils.waitTill(5000);	
			if(filepresentinlist.contains(filenamesearched)&&numberoffiles==1)	
			{
				
				    Log.message("Search with all attribute is validated!!!!!");
					return true;
					
			}
		else {
					Log.message("Search with all attribute is not  validated");
					return false;
			}	
	
   }
    /**Method to update attribute after adding
     * @throws AWTException 
     * 
     */
    public boolean modifyAddedAttributes(String projectno,String vendorname) throws AWTException {
    	
    	
		SkySiteUtils.waitTill(4000);
		CoustomPropertiestab.click();
		Log.message("Custome Properties tab clicked");
		SkySiteUtils.waitTill(4000);
		SkySiteUtils.waitForElement(driver,Projectnofield,40);
		Projectnofield.click();
		Log.message("Project no field is clicked");
		SkySiteUtils.waitTill(3000);
		//Projectnofield.clear();
		//Log.message("Project no field is cleared");
		//SkySiteUtils.waitTill(4000);
		Projectnofield.clear();
		Log.message("Project no field is cleared");
		Projectnofield.sendKeys(projectno);
		Log.message("Project number entered is " +projectno);
		SkySiteUtils.waitTill(3000);
		vendorinputboxupdate.click();
		Log.message("Vendor input box clicked");
	//	VendorInputBox.clear();
		//Log.message("Vendor box is cleard");
		Robot rb1=new Robot();
		rb1.keyPress(KeyEvent.VK_CONTROL);
		rb1.keyPress(KeyEvent.VK_A);
	
		rb1.keyRelease(KeyEvent.VK_A);
		rb1.keyRelease(KeyEvent.VK_CONTROL);
		rb1.keyPress(KeyEvent.VK_BACK_SPACE);
		rb1.keyRelease(KeyEvent.VK_BACK_SPACE);
		Log.message("vendor input box is cleared");
		SkySiteUtils.waitTill(4000);
		Actions act1=new Actions(driver);
		act1.sendKeys(vendorname).build().perform();
		//vendorinputboxupdate.sendKeys(vendorname);
		Log.message("Vendor name  is entered");
		SkySiteUtils.waitTill(2000);
		Robot rb= new Robot();
		rb.keyPress(KeyEvent.VK_ENTER);
		rb.keyRelease(KeyEvent.VK_ENTER);

		//selectVendoricon.click();
	
		SkySiteUtils.waitTill(4000);
	
 		updateclosebtn.click();
		Log.message("update button is clicked");
		SkySiteUtils.waitTill(4000);

		int numberoffiles=driver.findElements(By.xpath(".//*[@id='divGrid']/div[2]/table/tbody/tr/td[1]/img")).size();
		Log.message("No of file present are " +numberoffiles);
		int i =0;
		for(i=1;i<=numberoffiles;i++) {
			String Projectnopresent=driver.findElement(By.xpath("(.//*[@id='divGrid']/div[2]/table/tbody/tr/td[12])["+i+"]")).getText();
			String vendorpresent=driver.findElement(By.xpath("(.//*[@id='divGrid']/div[2]/table/tbody/tr/td[11])["+i+"]")).getText();
				
			if(Projectnopresent.contains(projectno)&&vendorpresent.contains(vendorname))	
			{
			
			    Log.message("File is present after searching with project number and vendor ");
				break;
				
			}
			}
		String Projectnopresent12=driver.findElement(By.xpath("(.//*[@id='divGrid']/div[2]/table/tbody/tr/td[12])["+i+"]")).getText();
		String vendorpresent12=driver.findElement(By.xpath("(.//*[@id='divGrid']/div[2]/table/tbody/tr/td[11])["+i+"]")).getText();
			
		if(Projectnopresent12.contains(projectno)&&vendorpresent12.contains(vendorname))	
		{
		
		    Log.message("File is present after searching with project number and vendor ");
		   return true;
			
		}
		else {
			
			
			Log.message("File is not present after search with project number and vendor");
			return false;
		}
		}
        @FindBy(xpath="//input[@placeholder='Project number']")
        WebElement projectsearchbox2;
    	
    	/**Method to search the update value 
    	 * @throws AWTException 
    	 * 
    	 */
		public boolean searchUpdatedValue(String updatedprojectno,String vendor) throws AWTException {

			closebtn.click();
			Log.message("Close button is clicked");
			SkySiteUtils.waitForElement(driver, advancedsearch,50);
			JavascriptExecutor executor = (JavascriptExecutor)driver;
			executor.executeScript("arguments[0].click();", advancedsearch);
	        Actions actclick12=new Actions(driver);
		   // actclick12.moveToElement(advancedsearch).click(advancedsearch).build().perform();
		    Log.message("Advanced search option is clikced");
		    SkySiteUtils.waitTill(6000);
			SkySiteUtils.waitForElement(driver, resetbtn, 40);
			resetbtn.click();
		
			Log.message("reset button is clicked");
			SkySiteUtils.waitTill(3000);
			
		//	String Projectnosearch=PropertyReader.getProperty("Project_Number");
		    WebElement Projectnobox=driver.findElement(By.xpath("//input[@placeholder='Project number']"));
		  
		
			WebElement vendorsearchbox=driver.findElement(By.xpath("//*[@tabindex='3']"));
			SkySiteUtils.waitForElement(driver,vendorsearchbox, 40);
		    SkySiteUtils.waitTill(4000);
		    actclick12.moveToElement(vendorsearchbox).click(vendorsearchbox).build().perform();
		    Log.message("Search box is clicked");
		    SkySiteUtils.waitTill(6000);
		    Actions act2=new Actions(driver);
		    act2.sendKeys(vendor).build().perform();
		    SkySiteUtils.waitTill(4000);
	
		    Log.message("Vendor searched is " +vendor);
		    SkySiteUtils.waitTill(6000);
		   
			Robot rb= new Robot();
			rb.keyPress(KeyEvent.VK_DOWN);
			rb.keyRelease(KeyEvent.VK_DOWN);
			rb.keyPress(KeyEvent.VK_ENTER);
			rb.keyRelease(KeyEvent.VK_ENTER);
			Log.message("Enter is pressed");
			//SkySiteUtils.waitTill(5000);
		//	SkySiteUtils.waitForElement(driver, projectsearchbox2, 40);
			//JavascriptExecutor executor1 = (JavascriptExecutor)driver;
			//executor1.executeScript("arguments[0].click();", );
	
			//act2.moveToElement(projectsearchbox2).click(projectsearchbox2).build().perform();
			//  Log.message("poject search box is clicked");
			//act2.sendKeys(updatedprojectno).build().perform();
			   
			//  Log.message("Project number entered is " +updatedprojectno);
			  SkySiteUtils.waitTill(5000);

			Searchbtn.click();
			Log.message("search button is clicked");
			SkySiteUtils.waitTill(4000);
			SkySiteUtils.waitForElement(driver,advancedsearch, 40);
			int numberoffiles=driver.findElements(By.xpath(".//*[@id='divGrid']/div[2]/table/tbody/tr/td[1]/img")).size();
			Log.message("No of file present are " +numberoffiles);
			String filenamesearched=PropertyReader.getProperty("Searchfilename");
			
			int i =0;
			for(i=1;i<=numberoffiles;i++) {
					
			String fileinlist =driver.findElement(By.xpath("(.//*[@id='divGrid']/div[2]/table/tbody/tr/td[2])["+i+"]")).getText();
					
			if(fileinlist.contains(filenamesearched)&&numberoffiles==1)	
			{
			
				  Log.message("File is present after searching with Project number and vendor");
					break;
					
		     }
		}
			String filepresentinlist=driver.findElement(By.xpath("(.//*[@id='divGrid']/div[2]/table/tbody/tr/td[2])["+i+"]")).getText();
				
			if(filepresentinlist.contains(filenamesearched)&&numberoffiles==1)	
			{
				
				    Log.message("Search with Project number and vendor is validated !!!!!");
					return true;
					
			}
		else {
					Log.message("Search with Project number and vendor is not validated");
					return false;
			}	
		}
		
				/**Search with Vendor
		 * 
		 */
		public boolean searchWithVendorwhenaddingattribute() throws AWTException {

			closebtn.click();
			Log.message("Close button is clicked");
			SkySiteUtils.waitForElement(driver, advancedsearch,50);
	        Actions actclick12=new Actions(driver);
		    actclick12.moveToElement(advancedsearch).click(advancedsearch).build().perform();
		    Log.message("Advanced search option is clikced");
			SkySiteUtils.waitForElement(driver, resetbtn, 40);
			resetbtn.click();
		
			Log.message("reset button is clicked");
			SkySiteUtils.waitTill(3000);
			
		
			SkySiteUtils.waitTill(5000);
			String Vendorforsearch=PropertyReader.getProperty("VendorforSearch");
			//WebElement vendorsearchbox=driver.findElement(By.xpath(".//*[@id='lookup_combo_Search_10174639']/div/input[1]"));
			WebElement vendorsearchbox=driver.findElement(By.xpath(".//*[@data-id='10178919']"));
			//for Production
		//	WebElement vendorsearchbox=driver.findElement(By.xpath(".//*[@data-id='10174639']"));
			SkySiteUtils.waitForElement(driver,vendorsearchbox, 40);
		    SkySiteUtils.waitTill(4000);
		    vendorsearchbox.click();
		    Log.message("Search box is clicked");
		    SkySiteUtils.waitTill(6000);
		    Actions act2=new Actions(driver);
		    act2.sendKeys(Vendorforsearch).build().perform();
		    SkySiteUtils.waitTill(4000);
	
		    Log.message("Vendor searched is " +Vendorforsearch);
		    SkySiteUtils.waitTill(6000);
		   
			Robot rb= new Robot();
			rb.keyPress(KeyEvent.VK_DOWN);
			rb.keyRelease(KeyEvent.VK_DOWN);
			rb.keyPress(KeyEvent.VK_ENTER);
			rb.keyRelease(KeyEvent.VK_ENTER);
			Log.message("Enter is pressed");
			SkySiteUtils.waitTill(5000);
			Searchbtn.click();
			Log.message("search button is clicked");
			SkySiteUtils.waitTill(4000);
			SkySiteUtils.waitForElement(driver,advancedsearch, 40);
			SkySiteUtils.waitTill(20000);
			String filenamesearched=PropertyReader.getProperty("Searchfilenameforaddedattribute");
			Log.message("File searched is " +filenamesearched);
			int numberoffiles=driver.findElements(By.xpath(".//*[@id='divGrid']/div[2]/table/tbody/tr/td[1]/img")).size();
			Log.message("No of file present are " +numberoffiles);
			SkySiteUtils.waitTill(9000);
			
			int i =0;
			for(i=1;i<=numberoffiles;i++) {
					
			String vendorname =driver.findElement(By.xpath("(.//*[@id='divGrid']/div[2]/table/tbody/tr[2]/td[11])["+i+"]")).getText();
					
			if(vendorname.contains("Airtel")&&numberoffiles==1)	
			{
			
				  Log.message("File is present after searching with Project number and vendor");
					break;
					
		     }
		}
			String vendorname12=driver.findElement(By.xpath("(.//*[@id='divGrid']/div[2]/table/tbody/tr[2]/td[11])["+i+"]")).getText();
				
			if(vendorname12.contains("Airtel")&&numberoffiles==1)	
			{
				
				    Log.message("Search with Project number and vendor is validated !!!!!");
					return true;
					
			}
		else {
					Log.message("Search with Project number and vendor is not validated");
					return false;
			}	
		
	}
}






    	
  

	
	

