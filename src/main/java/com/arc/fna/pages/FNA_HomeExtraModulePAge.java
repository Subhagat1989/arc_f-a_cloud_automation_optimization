package com.arc.fna.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

import com.arc.fna.utils.SkySiteUtils;
import com.arcautoframe.utils.Log;

public class FNA_HomeExtraModulePAge  extends LoadableComponent<FNA_HomeExtraModulePAge> {
	 WebDriver driver;
	    private  boolean isPageLoaded;
	    

	    @Override
		protected void load() {
			// TODO Auto-generated method stub
	
			
			isPageLoaded = true;
			SkySiteUtils.waitForElement(driver, albumicon, 10);
		
		}

		@Override
		protected void isLoaded() throws Error {
			// TODO Auto-generated method stub
			if (!isPageLoaded)
			{
				Assert.fail();
			}
		}
	
		/**
		 * Declaring constructor for initializing web elements using PageFactory class.
		 * @param driver
		 */
		public  FNA_HomeExtraModulePAge(WebDriver driver){
			this.driver=driver;
			PageFactory.initElements(driver, this);
		}
	
	  /************Method for album module***********/
	@FindBy(css="#ProjectMenu1_Album>a")
	@CacheLookup
	WebElement albumicon;
	public FNAAlbumPage clickAlbumIcon() {
		driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(4000);
		albumicon.click();
		Log.message("Album icon is clicked");
		SkySiteUtils.waitTill(5000);
		return new FNAAlbumPage(driver).get();
		
	}
   

}
